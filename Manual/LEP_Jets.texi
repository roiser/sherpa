@node LEP_Jets
@subsection Jets at lepton colliders

@menu
* LEP_Jets_LOmerging::
* LEP_Jets_NLOmerging::
@end menu

This section contains two setups to describe jet production at LEP I, 
either through multijet merging at leading order accuracy or at 
next-to-leading order accuracy.

@node LEP_Jets_LOmerging
@subsubsection MEPS setup for ee->jets

This example shows a LEP set-up, with electrons and positrons colliding 
at a centre of mass energy of 91.2 GeV.

@example
@smallformat
@verbatiminclude Examples/Jets_at_LeptonColliders/LEP_Jets/Sherpa.yaml
@end smallformat
@end example

Things to notice:
@itemize @bullet
@item The running 
of alpha_s is set to leading order and the value of alpha_s at the Z-mass 
is set.
@item Note that initial-state radiation is enabled by default. See @ref{ISR
Parameters} on how to disable it if you want to evaluate the (unphysical) case
where the energy for the incoming leptons is fixed.
@end itemize

@node LEP_Jets_NLOmerging
@subsubsection MEPS@@NLO setup for ee->jets

This example expands upon the above setup, elevating its description 
of hard jet production to next-to-leading order.

@example
@smallformat
@verbatiminclude Examples/Jets_at_LeptonColliders/LEP_Jets/Sherpa.NLO.yaml
@end smallformat
@end example

Things to notice:
@itemize @bullet
@item the b-quark mass has been enabled for the matrix element calculation
(the default is massless) because it is not negligible for LEP energies
@item the b b-bar and b b b-bar b-bar processes are specified separately because the 
@option{93} particle container contains only partons set massless in the matrix
element calculation, see @ref{Particle containers}. 
@item model parameters can be modified in the config file; in this example, the
value of alpha_s at the Z mass is set.
@end itemize

