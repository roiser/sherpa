@node Input structure
@chapter Input structure
@cindex PATH
@cindex RUNDATA

A Sherpa setup is steered by various parameters, associated with the
different components of event generation.

These have to be specified in a
configuration file which by default is named @code{Sherpa.yaml} in the current
working directory.
If you want to use a different setup directory for your Sherpa run, you have to
specify it on the command line as @code{-p <dir>} or @code{'PATH: <dir>'}
(including the quotes).
To read parameters from a configuration file with a different name, you may
specify @code{-f <file>} or @code{'RUNDATA: <file>'}.

Sherpa's configuration files are writtin in the @uref{yaml.org,,YAML} format.
Most settings are just written as the settings' name followed by its value,
like this:
@verbatim
  EVENTS: 100M
  BEAMS: 2212
  BEAM_ENERGIES: 7000
  ...
@end verbatim

Others use a more nested structure:
@verbatim
  HARD_DECAYS:
    Enabled: true
    Apply_Branching_Ratios: false
@end verbatim

where @code{Enabled} and @code{Apply_Branching_Ratios} are sub-settings of the
top-level @code{HARD_DECAYS} setting, which is denoted by indentation (here two
additional spaces).

The different settings and their structure are described in detail in another
chapter of this
manual, see @ref{Parameters}.

All parameters can be overwritten on the command line, i.e.
command-line input has the highest priority.
Each argument is parsed as a single YAML line. This usually means that you have
to quote each argument:
@verbatim
  <prefix>/bin/Sherpa 'KEYWORD1: value1' 'KEYWORD2: value2' ...
@end verbatim

Because each argument is parsed as YAML, you can also specify nested settings,
e.g. to disable hard decays (even if it is enabled in the config file) you can
write:
@verbatim
  <prefix>/bin/Sherpa 'HARD_DECAYS: {Enabled: false}'
@end verbatim

Or you can specify the list of matrix-element generators writing:
@verbatim
  <prefix>/bin/Sherpa 'ME_GENERATORS: [Comix, Amegic]'
@end verbatim

All over Sherpa, particles are defined by the particle code proposed by the
PDG. These codes and the particle properties will be listed during each run with
@code{OUTPUT: 2} for the elementary particles and @code{OUTPUT: 4} for the hadrons.
In both cases, antiparticles are characterized by a minus sign in front of their
code, e.g. a mu- has code @code{13}, while a mu+ has @code{-13}.

All quantities have to be specified in units of GeV and millimeter. The same
units apply to all numbers in the event output (momenta, vertex positions).
Scattering cross sections are denoted in pico-barn in the output.

There are a few extra features for an easier handling of the parameter
file(s), namely global tag replacement, see @ref{Tags}, and algebra
interpretation, see @ref{Interpreter}.


@menu
* Interpreter::      How to use the internal interpreter
* Tags::             How to use tags
@end menu

@node Interpreter
@section Interpreter

Sherpa has a built-in interpreter for algebraic expressions, like @samp{cos(5/180*M_PI)}.
This interpreter is employed when reading integer and floating point numbers from
input files, such that certain parameters can be written in a more convenient fashion.
For example it is possible to specify the factorisation scale as @samp{sqr(91.188)}.
@*
There are predefined tags to alleviate the handling

@table @samp

@item M_PI
Ludolph's Number to a precision of 12 digits.
@item M_C
The speed of light in the vacuum.
@item E_CMS
The total centre of mass energy of the collision.

@end table
The expression syntax is in general C-like, except for the extra function @samp{sqr},
which gives the square of its argument. Operator precedence is the same as in C.
The interpreter can handle functions with an arbitrary list of parameters, such as
@samp{min} and @samp{max}.
@*
The interpreter can be employed to construct arbitrary variables from four momenta,
like e.g. in the context of a parton level selector, see @ref{Selectors}.
The corresponding functions are

@table @samp

@item Mass(@var{v})
The invariant mass of @var{v} in GeV.
@item Abs2(@var{v})
The invariant mass squared of @var{v} in GeV^2.
@item PPerp(@var{v})
The transverse momentum of @var{v} in GeV.
@item PPerp2(@var{v})
The transverse momentum squared of @var{v} in GeV^2.
@item MPerp(@var{v})
The transverse mass of @var{v} in GeV.
@item MPerp2(@var{v})
The transverse mass squared of @var{v} in GeV^2.
@item Theta(@var{v})
The polar angle of @var{v} in radians.
@item Eta(@var{v})
The pseudorapidity of @var{v}.
@item Y(@var{v})
The rapidity of @var{v}.
@item Phi(@var{v})
The azimuthal angle of @var{v} in radians.

@item Comp(@var{v},@var{i})
The @var{i}'th component of the vector @var{v}. @var{i}=0 is the 
energy/time component, @var{i}=1, 2, and 3 are the x, y, and z 
components.
@item PPerpR(@var{v1},@var{v2})
The relative transverse momentum between @var{v1} and @var{v2} in GeV.
@item ThetaR(@var{v1},@var{v2})
The relative angle between @var{v1} and @var{v2} in radians.
@item DEta(@var{v1},@var{v2})
The pseudo-rapidity difference between @var{v1} and @var{v2}.
@item DY(@var{v1},@var{v2})
The rapidity difference between @var{v1} and @var{v2}.
@item DPhi(@var{v1},@var{v2})
The relative polar angle between @var{v1} and @var{v2} in radians.

@end table


@node Tags
@section Tags

Tag replacement in Sherpa is performed through the data
reading routines, which means that it can be performed for
virtually all inputs.
Specifying a tag on the command line or in the configuration file using the
syntax @code{TAGS: @{<Tag>: <Value>@}} will replace every occurrence of
@code{@@(<Tag>)} in all files
during read-in. An example tag definition could read
@verbatim
  <prefix>/bin/Sherpa 'TAGS: {QCUT: 20, NJET: 3}'
@end verbatim

and then be used in the configuration file like:
@verbatim
  RESULT_DIRECTORY: Result_$(QCUT);
  PROCESSES:
    - Process: "93 93 -> 11 -11 93{$(NJET)}"
      Order: {QCD: Any, EW: 2}
      CKKW: $(QCUT)
@end verbatim
